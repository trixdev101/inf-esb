package com.trix.transfer.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ResourceFileInputStream {

    private static final Log log = LogFactory.getLog(ResourceFileInputStream.class);

    public static final InputStream loadResourceFile(final String file) throws FileNotFoundException {
        return new FileInputStream(System.getProperty("user.home") + File.separator +"secure" + File.separator + file);
    }
}