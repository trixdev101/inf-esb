package com.trix.transfer.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import com.trix.transfer.annotations.SecureParam;

public class ReflectField {
    public static<T> T get(Object obj, String fieldPath) {
        return (T) getValue(obj, fieldPath);
    }
    private static Object getValue(Object obj, String fieldPath) {
        Object res = obj;
        try {
            for(Field field : res.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if(field.isAnnotationPresent(SecureParam.class)) {
                    Annotation annotated = field.getAnnotation(SecureParam.class);
                    SecureParam param = (SecureParam) annotated;
                    if(param.value().equals(fieldPath)) {
                        return field.get(obj);
                    }
                }
            }
        } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }
}