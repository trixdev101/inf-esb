package com.trix.transfer.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.trix.transfer.fields.SecureParamField;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.protocol.RequestAddCookies;
import org.apache.http.client.protocol.RequestDefaultHeaders;
import org.apache.http.client.protocol.RequestExpectContinue;
import org.apache.http.client.protocol.RequestProxyAuthentication;
import org.apache.http.client.protocol.RequestTargetAuthentication;
import org.apache.http.client.protocol.ResponseProcessCookies;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;

/**
 * @author Teguh Santoso
 */
public class SslHttpUtil {

    private static final Log log = LogFactory.getLog(SslHttpUtil.class);

    public static final CloseableHttpClient createSslHttpClient(SecureParamField param) {
        try {
            KeyStore trustStore = KeyStore.getInstance(param.getInstance());
            InputStream instream = ResourceFileInputStream.loadResourceFile(param.getKeyFile());
            
            trustStore.load(instream, param.getPassword().toCharArray());
            instream.close();

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(trustStore, param.getPassword().toCharArray());
            KeyManager[] kms = kmf.getKeyManagers();
            TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager(){
                
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        // TODO Auto-generated method stub
                        return null;
                    }
                
                    @Override
                    public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                        // TODO Auto-generated method stub
                        
                    }
                
                    @Override
                    public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                        // TODO Auto-generated method stub
                        
                    }
                }
            };
           
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kms, trustAllCerts, new SecureRandom());
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
                
            HttpResponseInterceptor contentEncodingFixerInterceptor = new HttpResponseInterceptor()  {
                @Override
                public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
                    Header contentEncodingHeader = response.getFirstHeader(HTTP.CONTENT_ENCODING);
                    if(contentEncodingHeader != null && contentEncodingHeader.getValue().equalsIgnoreCase("none")) {
                        response.removeHeaders(HTTP.CONTENT_ENCODING);
                        response.addHeader(HTTP.CONTENT_ENCODING, "identity");
                    }
                }
            };
            BasicHttpProcessor httpProcessor = new BasicHttpProcessor();

            httpProcessor.addInterceptor(contentEncodingFixerInterceptor);

            // Required protocol interceptors
            httpProcessor.addInterceptor(new RequestContent());
            httpProcessor.addInterceptor(new RequestTargetHost());
            httpProcessor.addInterceptor(new RequestDefaultHeaders());

            // Recommended protocol interceptors
            httpProcessor.addInterceptor(new RequestConnControl());
            httpProcessor.addInterceptor(new RequestUserAgent());
            httpProcessor.addInterceptor(new RequestExpectContinue());

            // HTTP state management interceptors
            httpProcessor.addInterceptor(new RequestAddCookies());
            httpProcessor.addInterceptor(new ResponseProcessCookies());

            // HTTP authentication interceptors
            // httpProcessor.addInterceptor(new RequestTargetAuthentication());
            // httpProcessor.addInterceptor(new RequestProxyAuthentication());    
            CloseableHttpClient httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .setHttpProcessor(httpProcessor)
                .build();
            return httpclient;
            
        } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }
}