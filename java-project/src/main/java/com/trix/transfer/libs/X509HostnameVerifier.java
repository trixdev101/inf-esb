package com.trix.transfer.libs;

import com.trix.transfer.fields.SecureParamField;
import com.trix.transfer.utils.ReflectField;
import com.trix.transfer.utils.SslHttpUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.impl.client.CloseableHttpClient;

public class X509HostnameVerifier {
    
    private static final Log log = LogFactory.getLog(X509HostnameVerifier.class);

    public static final CloseableHttpClient tlsHttpClient( Object obj){
        SecureParamField paramField = new SecureParamField();
        try {
            paramField.setInstance((String)ReflectField.get(obj, "instance"));
            paramField.setKeyFile((String)ReflectField.get(obj, "keyFile"));
            paramField.setPassword((String)ReflectField.get(obj, "password"));
            return SslHttpUtil.createSslHttpClient(paramField);
        } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
        }
        
        return null;
    }

}