package com.trix.transfer.commons;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * Hack to get DELETE to accept a request body.
 */
class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
	public static final String METHOD_NAME = "DELETE";

	@Override
	public String getMethod() {
		return METHOD_NAME;
	}

	public HttpDeleteWithBody(final String uri) {
		super();
		setURI(URI.create(uri));
	}
}


/**
 * Class Client allows for quick and easy access any REST or REST-like API.
 */
public class Client implements Closeable {

	private final CloseableHttpClient httpClient;
    private final Boolean secure;
    // private final boolean createdHttpClient;

    private RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(5 * 1000).build();

    /**
     * Constructor for using the default CloseableHttpClient.
     */
    public Client() {
    	
    	/*
    	RequestConfig requestConfig = RequestConfig.custom()
		        .setConnectTimeout(5000)
		        .setSocketTimeout(5000)
		        .build();
		SocketConfig socketConfig = SocketConfig.custom()
		        .setSoTimeout(5000)
		        .build();

		this.httpClient = HttpClients.custom()
		        .setDefaultRequestConfig(requestConfig)
		        .setDefaultSocketConfig(socketConfig)
		        .build();
		        */
		
        this.httpClient = HttpClients.createDefault();


        this.secure = false;
        // this.createdHttpClient = true;
    }

    /**
     * Constructor for passing in an httpClient, typically for mocking. Passed-in
     * httpClient will not be closed by this Client.
     *
     * @param httpClient an Apache CloseableHttpClient
     */
    public Client(final CloseableHttpClient httpClient) {
        this(httpClient, false);
    }

    /**
     * Constructor for passing in a secure parameter to allow for http calls.
     *
     * @param secure is a Bool
     */
    public Client(final Boolean secure) {
        this(HttpClients.createDefault(), secure);
    }

    /**
     * Constructor for passing in an httpClient and secure parameter to allow for http
     * calls.
     *
     * @param httpClient an Apache CloseableHttpClient
     * @param secure       is a Bool
     */
    public Client(final CloseableHttpClient httpClient, final Boolean secure) {
        
        this.httpClient = httpClient;
        this.secure = secure;
        // this.createdHttpClient = true;
    }

    /**
     * Add query parameters to a URL.
     *
     * @param baseUri     (e.g. "api.sendgrid.com")
     * @param endpoint    (e.g. "/your/endpoint/path")
     * @param queryParams map of key, values representing the query parameters
     * @throws URISyntaxException in of a URI syntax error
     */
    public URI buildUri(final String baseUri, final String endpoint, final Map<String, String> queryParams)
            throws URISyntaxException {
        final URIBuilder builder = new URIBuilder();
        URI uri = null;

        if (this.secure == true) {
            builder.setScheme("https");
        } else {
            builder.setScheme("http");
        }

        builder.setHost(baseUri);
        builder.setPath(endpoint);

        if (queryParams != null) {
            for (final Map.Entry<String, String> entry : queryParams.entrySet()) {
                builder.setParameter(entry.getKey(), entry.getValue());
            }
        }

        try {
            uri = builder.build();
        } catch (final URISyntaxException ex) {
            throw ex;
        }

        return uri;
    }

    /**
     * Prepare a Response object from an API call via Apache's HTTP client.
     *
     * @param response from a call to a CloseableHttpClient
     * @throws IOException in case of a network error
     * @return the response object
     */
    public Response getResponse(final CloseableHttpResponse response) throws IOException {
        // final ResponseHandler<String> handler = new SendGridResponseHandler();
        final ResponseHandler<String> handler = new ResponseHandler<String>() {
            @Override
            public String handleResponse(
                    final HttpResponse response) throws IOException {
                final HttpEntity entity = response.getEntity();
                return entity == null ? null : handleEntity(entity);
            }
            
            private String handleEntity(HttpEntity entity) throws IOException {
                return EntityUtils.toString(entity, StandardCharsets.UTF_8);
            }
        };
        final String responseBody = handler.handleResponse(response);

        final int statusCode = response.getStatusLine().getStatusCode();

        final Header[] headers = response.getAllHeaders();
        final Map<String, String> responseHeaders = new HashMap<String, String>();
        for (final Header h : headers) {
            responseHeaders.put(h.getName(), h.getValue());
        }

        return new Response(statusCode, responseBody, responseHeaders);
    }

    /**
     * Make a GET request and provide the status code, response body and response
     * headers.
     * 
     * @param request the request object
     * @throws URISyntaxException in case of a URI syntax error
     * @throws IOException        in case of a network error
     * @return the response object
     */
    public Response get(final Request request) throws URISyntaxException, IOException {
        URI uri = null;
        HttpGet httpGet = null;

        try {
            uri = buildUri(request.getBaseUri(), request.getEndpoint(), request.getQueryParams());
            httpGet = new HttpGet(uri.toString());
            httpGet.setConfig(requestConfig);
        } catch (final URISyntaxException ex) {
            throw ex;
        }

        if (request.getHeaders() != null) {
            for (final Map.Entry<String, String> entry : request.getHeaders().entrySet()) {
                httpGet.setHeader(entry.getKey(), entry.getValue());
            }
        }
        return executeApiCall(httpGet);
    }

    /**
     * Make a POST request and provide the status code, response body and response
     * headers.
     *
     * @param request the request object
     * @throws URISyntaxException in case of a URI syntax error
     * @throws IOException        in case of a network error
     * @return the response object
     */
    public Response post(final Request request) throws URISyntaxException, IOException {
    	
		
  
        URI uri = null;
        HttpPost httpPost = null;

        try {
            uri = buildUri(request.getBaseUri(), request.getEndpoint(), request.getQueryParams());
            httpPost = new HttpPost(uri.toString());
        
        } catch (final URISyntaxException ex) {
            throw ex;
        }

        if (request.getHeaders() != null) {
            for (final Map.Entry<String, String> entry : request.getHeaders().entrySet()) {
                httpPost.setHeader(entry.getKey(), entry.getValue());
            }
        }
        
      
		

        //httpPost.setConfig(requestConfig);
        if(request.getBody() != null) httpPost.setEntity(new StringEntity(request.getBody(), Charset.forName("UTF-8")));
        if(request.getFormData().size() > 0) httpPost.setEntity(request.encodedFormData());
        writeContentTypeIfNeeded(request, httpPost);

        return executeApiCall(httpPost);
    }

    /**
     * Make a PATCH request and provide the status code, response body and response
     * headers.
     *
     * @param request the request object
     * @throws URISyntaxException in case of a URI syntax error
     * @throws IOException        in case of a network error
     * @return the response object
     */
    public Response patch(final Request request) throws URISyntaxException, IOException {
        URI uri = null;
        HttpPatch httpPatch = null;

        try {
            uri = buildUri(request.getBaseUri(), request.getEndpoint(), request.getQueryParams());
            httpPatch = new HttpPatch(uri.toString());
        } catch (final URISyntaxException ex) {
            throw ex;
        }

        if (request.getHeaders() != null) {
            for (final Map.Entry<String, String> entry : request.getHeaders().entrySet()) {
                httpPatch.setHeader(entry.getKey(), entry.getValue());
            }
        }
        httpPatch.setConfig(requestConfig);
        if(request.getBody() != null)  httpPatch.setEntity(new StringEntity(request.getBody(), Charset.forName("UTF-8")));
        if(request.getFormData().size() > 0) httpPatch.setEntity(request.encodedFormData());
        writeContentTypeIfNeeded(request, httpPatch);

        return executeApiCall(httpPatch);
    }

    /**
     * Make a PUT request and provide the status code, response body and response
     * headers.
     *
     * @param request the request object
     * @throws URISyntaxException in case of a URI syntax error
     * @throws IOException        in case of a network error
     * @return the response object
     */
    public Response put(final Request request) throws URISyntaxException, IOException {
        URI uri = null;
        HttpPut httpPut = null;

        try {
            uri = buildUri(request.getBaseUri(), request.getEndpoint(), request.getQueryParams());
            httpPut = new HttpPut(uri.toString());
        } catch (final URISyntaxException ex) {
            throw ex;
        }

        if (request.getHeaders() != null) {
            for (final Map.Entry<String, String> entry : request.getHeaders().entrySet()) {
                httpPut.setHeader(entry.getKey(), entry.getValue());
            }
        }
        httpPut.setConfig(requestConfig);
        if(request.getBody() != null)  httpPut.setEntity(new StringEntity(request.getBody(), Charset.forName("UTF-8")));
        if(request.getFormData().size() > 0) httpPut.setEntity(request.encodedFormData());
        writeContentTypeIfNeeded(request, httpPut);

        return executeApiCall(httpPut);
    }

    /**
     * Make a DELETE request and provide the status code and response headers.
     *
     * @param request the request object
     * @throws URISyntaxException in case of a URI syntax error
     * @throws IOException        in case of a network error
     * @return the response object
     */
    public Response delete(final Request request) throws URISyntaxException, IOException {
        URI uri = null;
        HttpDeleteWithBody httpDelete = null;

        try {
            uri = buildUri(request.getBaseUri(), request.getEndpoint(), request.getQueryParams());
            httpDelete = new HttpDeleteWithBody(uri.toString());
        } catch (final URISyntaxException ex) {
            throw ex;
        }

        if (request.getHeaders() != null) {
            for (final Map.Entry<String, String> entry : request.getHeaders().entrySet()) {
                httpDelete.setHeader(entry.getKey(), entry.getValue());
            }
        }
        httpDelete.setConfig(requestConfig);
        if(request.getBody() != null)  httpDelete.setEntity(new StringEntity(request.getBody(), Charset.forName("UTF-8")));
        if(request.getFormData().size() > 0) httpDelete.setEntity(request.encodedFormData());
        writeContentTypeIfNeeded(request, httpDelete);

        return executeApiCall(httpDelete);
    }

    private void writeContentTypeIfNeeded(final Request request, final HttpMessage httpMessage) {
        if (!"".equals(request.getBody())) {
            httpMessage.setHeader("Content-Type", "application/json");
        }
    }

    /**
     * Makes a call to the client API.
     *
     * @param httpPost the request method object
     * @throws IOException in case of a network error
     * @return the response object
     */
    private Response executeApiCall(final HttpRequestBase httpPost) throws IOException {
        try {
            
            final CloseableHttpResponse serverResponse = httpClient.execute(httpPost);
            try {
                return getResponse(serverResponse);
            } finally {
                serverResponse.close();
            }
        } catch (final ClientProtocolException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }

    /**
     * A thin wrapper around the HTTP methods.
     *
     * @param request the request object
     * @throws IOException in case of a network error
     * @return the response object
     */
    public Response api(final Request request) throws IOException {
        try {
            if (request.getMethod() == null) {
                throw new IOException("We only support GET, PUT, PATCH, POST and DELETE.");
            }
            switch (request.getMethod()) {
                case GET:
                    return get(request);
                case POST:
                    return post(request);
                case PUT:
                    return put(request);
                case PATCH:
                    return patch(request);
                case DELETE:
                    return delete(request);
                default:
                    throw new IOException("We only support GET, PUT, PATCH, POST and DELETE.");
            }
        } catch (final IOException ex) {
            throw ex;
        } catch (final URISyntaxException ex) {
            final StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            throw new IOException(errors.toString());
        }
    }

    /**
     * Closes the http client.
     *
     * @throws IOException in case of a network error
     */
    @Override
    public void close() throws IOException {
        this.httpClient.close();
    }

    /**
     * Closes and finalizes the http client.
     *
     * @throws Throwable in case of an error
     */
    @Override
    public void finalize() throws Throwable {
        try {
            close();
        } catch (final IOException e) {
			throw new Throwable(e.getMessage());
		} finally {
			super.finalize();
		}
	}
}