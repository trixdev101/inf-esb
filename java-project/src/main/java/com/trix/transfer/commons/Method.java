package com.trix.transfer.commons;

public enum Method {
    GET, POST, PATCH, PUT, DELETE
}