package com.trix.transfer.fields;

public class SecureParamField {
    private String instance;
    private String keyFile;
    private String password;

    public String getInstance() {
        return instance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeyFile() {
        return keyFile;
    }

    public void setKeyFile(String keyFile) {
        this.keyFile = keyFile;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    @Override
    public String toString() {
        return instance + " " + keyFile + " " + password;
    }
}