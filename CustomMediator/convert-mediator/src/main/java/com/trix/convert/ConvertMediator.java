package com.trix.convert;

import org.apache.synapse.mediators.AbstractMediator;
import org.apache.synapse.MessageContext;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Hello world!
 *
 */
public class ConvertMediator extends AbstractMediator {
    private static final Log log = LogFactory.getLog(ConvertMediator.class);

    private String value;
    private String pattern;
    private String to;
    private String timezone;
    private String volume;

    @Override
    public boolean mediate(MessageContext synCtx) {
        try {
            SimpleDateFormat dt = null;
            if (pattern != null)
                dt = new SimpleDateFormat(pattern);
            else
                dt = new SimpleDateFormat();
            String formated = "";
            if (to.equals("milliseconds")) {
                Date converted = dt.parse(value);
                formated = String.valueOf(converted.getTime());
            } else if (to.equals("utc")) {
                dt.setTimeZone(TimeZone.getTimeZone("UTC"));
                formated = dt.format(new Date(Long.parseLong(value)));
            } else if (to.equals("gmt")) {
                if (timezone != null) {
                    TimeZone tz = TimeZone.getTimeZone(timezone);
                    dt.setTimeZone(tz);
                    Date converted = new Date(Long.parseLong(value));
                    formated = dt.format(converted);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss.SSS");
                    Date converted = sdf.parse(value);
                    formated = dt.format(converted);
                }
            } else if (to.equals("calculated")) {
                Long calculated = 0L;
                log.info("reduce " + volume);
                Date parse = dt.parse(value);
                String[] splitVolumeStr = volume.split(" ");
                String timeToRecude = splitVolumeStr[0];
                String timer = splitVolumeStr[1];
                if (timeToRecude.startsWith("-")) {
                    if(timer.equals("hour")){
                        calculated = parse.getTime() - 
                            (Integer.parseInt(timeToRecude.replace("-", "")) * 60 * 60 * 1000);
                    } else if (timer.equals("minutes")) {
                        calculated = parse.getTime() - 
                            (Integer.parseInt(timeToRecude.replace("-", "")) * 60 * 1000);
                    } else {
                        calculated = parse.getTime() - 
                            (Integer.parseInt(timeToRecude.replace("-", "")) * 1000);
                    }
                } else {
                    if(timer.equals("hour")){
                        calculated = parse.getTime() + 
                            (Integer.parseInt(timeToRecude.replace("+", "")) * 60 * 60 * 1000);
                    } else if (timer.equals("minutes")) {
                        calculated = parse.getTime() + 
                            (Integer.parseInt(timeToRecude.replace("+", "")) * 60 * 1000);
                    } else {
                        calculated = parse.getTime() + 
                            (Integer.parseInt(timeToRecude.replace("+", "")) * 1000);
                    }
                }
                Date converted = new Date(calculated);
                formated = dt.format(converted);
            } else {
                Date converted = new Date(Long.parseLong(value));
                formated = dt.format(converted);
            }
            synCtx.setProperty("date.change", formated);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String val){
        this.value = val;
    }

    public String getPattern() {
        return this.pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getTo(){
        return this.to;
    }

    public void setTo(String to){
        this.to = to;
    }
}
