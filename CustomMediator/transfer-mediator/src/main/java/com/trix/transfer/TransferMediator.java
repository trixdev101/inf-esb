package com.trix.transfer;

import com.google.gson.Gson;
import com.trix.transfer.annotations.SecureParam;
import com.trix.transfer.commons.*;
import com.trix.transfer.libs.X509HostnameVerifier;
import com.trix.transfer.utils.CommonUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.net.URI;

import javax.ws.rs.HttpMethod;

import org.apache.commons.httpclient.HttpStatus;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.synapse.ManagedLifecycle;
import org.apache.synapse.MessageContext;
import org.apache.synapse.SynapseLog;
import org.apache.synapse.commons.json.JsonUtil;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.core.axis2.Axis2MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class TransferMediator extends AbstractMediator implements ManagedLifecycle {

    //private static final Log log = LogFactory.getLog(TransferMediator.class);

    /***
     * Serial host configuration
     */
    private String host = "";

    /**
     * Method transfer configuration
     */
    private String method = "GET";

    private String isTls = "false";

    @SecureParam("instance")
    private String instance;

    @SecureParam("keyFile")
    private String keyFile;

    @SecureParam("password")
    private String password;

    private String encodeUrl = "false";

    public TransferMediator() {
    }

    public String getEncodeUrl() {
        return encodeUrl;
    }

    public void setEncodeUrl(String encodeUrl) {
        this.encodeUrl = encodeUrl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeyFile() {
        return keyFile;
    }

    public void setKeyFile(String keyFile) {
        this.keyFile = keyFile;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getIsTls() {
        return isTls;
    }

    public void setIsTls(String isTls) {
        this.isTls = isTls;
    }

    @Override
    public boolean mediate(MessageContext synCtx) {
        return processResponseMessage(synCtx, getLog(synCtx));
    }

    @Override
    public void init(SynapseEnvironment se) {
        // TODO
    }

    @Override
    public void destroy() {
        // TODO
    }

    private Boolean processResponseMessage(MessageContext synCtx, SynapseLog synLog)  {
        String jsonPayloadToString = JsonUtil.jsonPayloadToString(((Axis2MessageContext) synCtx)
                             .getAxis2MessageContext());

        Map<String, String> headers = (Map) ((Axis2MessageContext) synCtx).getAxis2MessageContext()
                            .getProperty(org.apache.axis2.context.MessageContext.TRANSPORT_HEADERS);
        
        if (headers == null || headers.isEmpty()) {
            log.info("No Transport Headers Found");
            return true;
        }
        boolean https = host.startsWith("https://"); 
        Client client = null;
        
        try {
            Request request = new Request();
            host =  host.replaceFirst("^(http[s]?://www\\.|http[s]?://|www\\.)", "").replaceFirst("/*$", "");
            log.info("host " + host);
            //URI myUri = URI.create(host); // bgjii : check with URI
            //if ( myUri != null )
            //{
	            Boolean isEncodeUrl = Boolean.parseBoolean(encodeUrl);  
	            if(isEncodeUrl) host = CommonUtils.encodeURIComponent(host);
	            request.setBaseUri(host);
	            for (Object name : headers.keySet()) {
	                final String key = name.toString();
	                final String value = headers.get(name).toString();
	                if( key.equals("Content-Type") 
	                    || key.equals("Accept-Encoding")
	                    || key.equals("API") 
	                    || key.equals("DataType")
	                    || key.equals("X-ACCESS-TOKEN")
	                    || key.equals("X_ENTITY_KEY"))
	                    request.addHeader(key, value);
	            }
	            request.setBody(jsonPayloadToString.replaceAll("([^,{:])\"([^,:]})", "$1\\\\\"$2"));
	            
	            Response resp = null;
	            
	            Boolean isTls = Boolean.parseBoolean(this.isTls);
	            if(isTls){
	                client = new Client(X509HostnameVerifier.tlsHttpClient(this), https);
	            } else {
	                client = new Client(https);
	            }
	            
	            switch (method) {
	                case HttpMethod.GET:
	                    if(request.getBody() != null ) {
	                        request.setBody(null);
	                    }
	                    log.info("request:" + request.getBody());
	                    resp =  client.get(request);
	                    break;
	                case HttpMethod.POST:
	                    log.info("request:" + request.getBody());
	                    resp =  client.post(request);
	                    break;
	                case HttpMethod.PUT:
	                    log.info("request:" + request.getBody());
	                    resp =  client.put(request);
	                    break;
	                case HttpMethod.DELETE:
	                    log.info("request:" + request.getBody());
	                    resp =  client.delete(request);
	                    break;
	                default:
	                    break;
	            }
	            //log.info(resp);
	            if ( resp != null ) // bgjii : check resp, esb hang when not described
	            {
		            if(resp.getStatusCode() == HttpStatus.SC_NOT_FOUND 
		                || resp.getStatusCode() == HttpStatus.SC_BAD_REQUEST
		                || resp.getStatusCode() == HttpStatus.SC_BAD_GATEWAY
		                || resp.getStatusCode() == HttpStatus.SC_FORBIDDEN
		                || resp.getStatusCode() == HttpStatus.SC_UNAUTHORIZED
		                ) { 
		                log.info("STATUS CODE " + resp.getStatusCode());
		                Map<String, String> mapBody = new HashMap<String, String>();
		                mapBody.put("ec", String.valueOf(resp.getStatusCode()));
		                mapBody.put("msg", resp.getBody());
		                String respbody = new Gson().toJson(mapBody);
		                log.info(respbody);
		                synCtx.setProperty("RESPONSE_BODY", respbody);
		            }  else {
		                String body = resp.getBody();
		                log.info(body);
		                synCtx.setProperty("RESPONSE_BODY", body);
		            }
	            }
	            else
	            {
	            	Map<String, String> mapBody = new HashMap<String, String>();
	                mapBody.put("ec", "666");
	                mapBody.put("msg", "Transfer Mediator : respond null");
	                String respbody = new Gson().toJson(mapBody);
	                synCtx.setProperty("RESPONSE_BODY", respbody);
	            }

            //}
            // org.apache.axis2.context.MessageContext axis2MessageContext = ((Axis2MessageContext) synCtx).getAxis2MessageContext();
            // OMNode currentBody = axis2MessageContext.getEnvelope().getBody().getFirstOMChild();
            // if (currentBody != null) {
            //     currentBody.discard();
            // }
            // OMElement omXML = AXIOMUtil.stringToOM(resp);
            // axis2MessageContext.getEnvelope().getBody().addChild(omXML.getFirstElement());  
                    
        } catch (IOException e) {
            log.error("Error while executing the http request", e);
            synCtx.setProperty("ERROR_MESSAGE", "Error while executing the http request");
        } catch(URISyntaxException e) {
            log.error("Error setting the API response to body", e);
            synCtx.setProperty("ERROR_MESSAGE", "Error setting the API response to body");
        } catch (Exception e) {
            e.printStackTrace();
            synCtx.setProperty("ERROR_MESSAGE", "Error setting the API response to body");
        }
        
        if ( client != null )
        {
            try {
				client.finalize();
	        	//client.close(); // bgjii : close the client
	        	client = null;
            }
            catch ( Exception e )
            {
                synCtx.setProperty("ERROR_MESSAGE", "Fail closing client");
            }
            catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Returning from  KerberosSecuredAPIInvocationMediator...!!");
        }
    	return true;
    }


    /**
     * @return String return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }


    /**
     * @return String return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

}
