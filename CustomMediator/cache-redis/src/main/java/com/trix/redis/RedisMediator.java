package com.trix.redis;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import org.apache.synapse.registry.Registry;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Set;

import com.trix.redis.constants.RedisClassMediatorConstants;

/**
 * @author Teguh Santoso
 *
 */
public class RedisMediator extends AbstractMediator {

    private static JedisPool jedisPool;
    private Registry registryInstance;

    @Override
    public boolean mediate(final MessageContext messageContext) {

        String redisHost;
        int redisPort;
        int redisTimeout;
        // String redisPassword;
        int redisDatabase;
        Jedis jedis = null;
        boolean isGet = false;
        boolean isSet = false;

        try {

            registryInstance = messageContext.getConfiguration().getRegistry();
            if (registryInstance != null) {
                final String redisHostString = RedisClassMediatorConstants.REDIS_SERVER_HOST;
                if (StringUtils.isNotEmpty(redisHostString)) {
                    redisHost = redisHostString.trim();
                } else {
                    log.error("Redis Hostname is not set in Registry configurations, hence skipping RedisClassMediator execution");
                    return false;
                }
                final String redisPortString = RedisClassMediatorConstants.REDIS_SERVER_PORT;
                if (StringUtils.isNotEmpty(redisPortString)) {
                    redisPort = Integer.parseInt(redisPortString.trim());
                } else {
                    //log.info("Redis Port is not set in Registry configuration, hence using default port as 6379");
                    redisPort = 6379;
                }

                final String redisTimeoutString = RedisClassMediatorConstants.REDIS_SERVER_TIMEOUT;
                if (StringUtils.isNotEmpty(redisTimeoutString)) {
                    redisTimeout = Integer.parseInt(redisTimeoutString.trim());
                } else {
                    log.info("Redis timeout is not set in Registry configuration, hence using default timeout as 2000");
                    redisTimeout = 2000;
                }

                final String redisPasswordString = RedisClassMediatorConstants.REDIS_SERVER_PASSWORD;
                // if (StringUtils.isNotEmpty(redisHostString)) {
                //     redisPassword = redisPasswordString.trim();
                // } else {
                //     log.error(
                //             "Redis Password is not set in Registry configurations, hence skipping RedisClassMediator execution");
                //     return false;
                // }

                final String redisDatabaseString = RedisClassMediatorConstants.REDIS_SERVER_DATABASE;
                if (StringUtils.isNotEmpty(redisDatabaseString)) {
                    redisDatabase = Integer.parseInt(redisDatabaseString.trim());
                } else {
                    log.info("Redis database is not set in Registry configuration, hence using default database as 0");
                    redisDatabase = 0;
                }

                final String redisGetKey = (String) messageContext
                        .getProperty(RedisClassMediatorConstants.REDIS_GET_KEY);
                if (StringUtils.isNotEmpty(redisGetKey)) {
                    isGet = true;
                }
                Set pros = messageContext.getPropertyKeySet();
                if (pros != null) {
                    pros.remove(RedisClassMediatorConstants.REDIS_GET_VALUE);
                    pros.remove(RedisClassMediatorConstants.REDIS_SET_VALUE_STATUS);
                }
                String redisSetKey = (String) messageContext.getProperty(RedisClassMediatorConstants.REDIS_SET_KEY);
                String redisSetValue = (String) messageContext.getProperty(RedisClassMediatorConstants.REDIS_SET_VALUE);
                if (StringUtils.isNotEmpty(redisSetKey) && StringUtils.isNotEmpty(redisSetValue)) {
                    isSet = true;
                }
                jedis = getRedisPool(redisHost, redisPort, redisTimeout, redisPasswordString, redisDatabase).getResource();
                if (jedis != null) {
                    jedis.connect();
                    if (isGet) {
                        String redisGetValue = jedis.get(redisGetKey);
                        if (StringUtils.isNotEmpty(redisGetValue)) {
                            messageContext.setProperty(RedisClassMediatorConstants.REDIS_GET_VALUE, redisGetValue);
                            //if (log.isDebugEnabled()) {
                            //    log.debug( String.format("Get [Key] %s and Get [Value] %s for [messageId] %s", redisGetKey, redisGetValue, messageContext.getMessageID()));
                            //}
                        } else {
                            // messageContext.setProperty(RedisClassMediatorConstants.REDIS_GET_VALUE, "empty");
                        	//log.warn(String.format("A Valid value for [key] %s not found in Redis for [messageId] %s", redisGetKey, messageContext.getMessageID()));    
                        }
                        if (pros != null) {
                            pros.remove(RedisClassMediatorConstants.REDIS_GET_KEY);
                        }
                    }
                    else if (isSet) {
                        String status;
                        Object ttlValueObj = messageContext
                                .getProperty(RedisClassMediatorConstants.REDIS_SET_TTL_VALUE);
                        if (ttlValueObj instanceof Integer) {
                            int redisSetTTLValue = (Integer) messageContext
                                    .getProperty(RedisClassMediatorConstants.REDIS_SET_TTL_VALUE);
                            status = jedis.setex(redisSetKey, redisSetTTLValue, redisSetValue);
                        } else {
                            status = jedis.set(redisSetKey, redisSetValue);
                        }
                        messageContext.setProperty(RedisClassMediatorConstants.REDIS_SET_VALUE_STATUS, status);
                        //if (log.isDebugEnabled()) {
                        //    log.debug(String.format("Set [Key] %s and Set [Value] %s for [messageId] %s", redisSetKey, redisSetValue, messageContext.getMessageID()));
                        //}
                        if (pros != null) {
                            pros.remove(RedisClassMediatorConstants.REDIS_SET_KEY);
                            pros.remove(RedisClassMediatorConstants.REDIS_SET_VALUE);
                            if (ttlValueObj != null) {
                                pros.remove(RedisClassMediatorConstants.REDIS_SET_TTL_VALUE);
                            }
                        }
                    } else {
                        log.error("Cannot find required Redis GET or SET Properties, skipping Redis Mediator");
                    }
                } else {
                    log.error("Unexpected error. Cannot initiate Jedis Resource. Check your jedis connection details.");
                }
            } else {
                log.error("Cannot initiate Registry to read config values.");
            }

        } catch (final Exception e) {
            e.printStackTrace();
            String error = "Error occurred while handling message in RedisClassMediator. " + e;
            handleException(error, messageContext);
        } finally {

            if (jedis != null) {
                jedis.disconnect();
                jedis.close();
            }
        }

        return true;
    }

    private synchronized JedisPool getRedisPool(final String host, final int port, final int timeout, final String password, final int database) {
        if (jedisPool != null) {
            return jedisPool;
        } else {
        	//log.info("getRedisPool");
        	if ( password.isEmpty() )
                jedisPool = new JedisPool(buildPoolConfig(), host, port, timeout);
        	else
                jedisPool = new JedisPool(buildPoolConfig(), host, port, timeout, password, database);
        		
        	//if (log.isDebugEnabled()) {
        	//    log.debug("Redis Connection Pool initialized");
        	//}
            return jedisPool;
        }
    }

    private GenericObjectPoolConfig buildPoolConfig() {
        
        int maxTotal = 128; //8
        int maxIdle = 128; // 8
        int minIdle = 16; // 0
        boolean testOnBorrow = true;
        boolean testOnReturn = true; //false
        boolean testWhileIdle = true; // false
        long minEvictableIdleTimeMillis = 60000;
        long timeBetweenEvictionRunsMillis = 30000; //-1
        int numTestsPerEvictionRun = 3;
        boolean blockWhenExhausted = true; // false

        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        
        final String maxTotalString = RedisClassMediatorConstants.REDIS_SERVER_MAX_TOTAL;
        final String maxIdleString =  RedisClassMediatorConstants.REDIS_SERVER_MAX_IDLE;
        final String minIdleString =  RedisClassMediatorConstants.REDIS_SERVER_MIN_IDLE;
        final String testOnBorrowString = RedisClassMediatorConstants.REDIS_SERVER_TEST_ON_BORROW;
        final String testOnReturnString = RedisClassMediatorConstants.REDIS_SERVER_TEST_ON_RETURN;
        final String testWhileIdleString = RedisClassMediatorConstants.REDIS_SERVER_TEST_WHILE_IDLE;
        final String minEvictableIdleTimeMillisString = RedisClassMediatorConstants.REDIS_SERVER_MIN_EVICT_IDL_TIME;
        final String timeBetweenEvictionRunsMillisString = RedisClassMediatorConstants.REDIS_SERVER_TIME_BW_EVCT_RUNS;
        final String numTestsPerEvictionRunString =  RedisClassMediatorConstants.REDIS_SERVER_NUM_TESTS_PER_EVCT_RUN;
        final String blockWhenExhaustedString = RedisClassMediatorConstants.REDIS_SERVER_BLOCK_WHEN_EXHAUSTED;

        if (StringUtils.isNotEmpty(maxTotalString)) {
            maxTotal = Integer.parseInt(maxTotalString.trim());
        }

        if (StringUtils.isNotEmpty(maxIdleString)) {
            maxIdle = Integer.parseInt(maxIdleString.trim());
        }

        if (StringUtils.isNotEmpty(minIdleString)) {
            minIdle = Integer.parseInt(minIdleString.trim());
        }

        if (StringUtils.isNotEmpty(testOnBorrowString)) {
            testOnBorrow = Boolean.parseBoolean(testOnBorrowString.trim());
        }

        if (StringUtils.isNotEmpty(testOnReturnString)) {
            testOnReturn = Boolean.parseBoolean(testOnReturnString.trim());
        }

        if (StringUtils.isNotEmpty(testWhileIdleString)) {
            testWhileIdle = Boolean.parseBoolean(testWhileIdleString.trim());
        }

        if (StringUtils.isNotEmpty(minEvictableIdleTimeMillisString)) {
            minEvictableIdleTimeMillis = Long.parseLong(minEvictableIdleTimeMillisString.trim());
        }

        if (StringUtils.isNotEmpty(timeBetweenEvictionRunsMillisString)) {
            timeBetweenEvictionRunsMillis = Long.parseLong(timeBetweenEvictionRunsMillisString.trim());
        }

        if (StringUtils.isNotEmpty(numTestsPerEvictionRunString)) {
            numTestsPerEvictionRun = Integer.parseInt(numTestsPerEvictionRunString.trim());
        }

        if (StringUtils.isNotEmpty(blockWhenExhaustedString)) {
            blockWhenExhausted = Boolean.parseBoolean(blockWhenExhaustedString.trim());
        }

        poolConfig.setMaxTotal(maxTotal);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setMinIdle(minIdle);
        poolConfig.setTestOnBorrow(testOnBorrow);
        poolConfig.setTestOnReturn(testOnReturn);
        poolConfig.setTestWhileIdle(testWhileIdle);
        poolConfig.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        poolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        poolConfig.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
        poolConfig.setBlockWhenExhausted(blockWhenExhausted);
        
        return poolConfig;
    }

    // private String getRegistryResourceString(final String registryPath) {
    //     String registryResourceContent = null;

    //     final Object obj = registryInstance.getResource(new Entry(registryPath), null);
    //     if (obj != null && obj instanceof OMTextImpl) {
    //         registryResourceContent = ((OMTextImpl) obj).getText();
    //     }
    //     return registryResourceContent;
    // }
}
