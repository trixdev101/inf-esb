package com.trix.redis.constants;

public class RedisClassMediatorConstants {
    // Redis Cluster Configuration values
    public static final String REDIS_SERVER_HOST = "redis.local";
    public static final String REDIS_SERVER_PORT = "6379";
    public static final String REDIS_SERVER_TIMEOUT = "5000";
    public static final String REDIS_SERVER_PASSWORD = "";
    //public static final String REDIS_SERVER_PASSWORD = "";
    public static final String REDIS_SERVER_DATABASE = "0";
    public static final String REG_PREFIX = "conf:/test/";

    // Redis Class mediator Properties
    public static final String REDIS_GET_KEY = "redis.key.get";
    public static final String REDIS_GET_VALUE = "redis.value.get";
    public static final String REDIS_SET_KEY = "redis.key.set";
    public static final String REDIS_SET_VALUE = "redis.value.set";
    public static final String REDIS_SET_VALUE_STATUS = "redis.set.status";
    public static final String REDIS_SET_TTL_VALUE = "redis.ttl.set";

    // Redis Connection Factory Configurations
    //public static final String REDIS_SERVER_MAX_TOTAL = "128";
    //public static final String REDIS_SERVER_MAX_IDLE = "128";
    //public static final String REDIS_SERVER_MIN_IDLE = "16";
    public static final String REDIS_SERVER_MAX_TOTAL = "160";
    public static final String REDIS_SERVER_MAX_IDLE = "80";
    public static final String REDIS_SERVER_MIN_IDLE = "0";
    public static final String REDIS_SERVER_TEST_ON_BORROW = "true";
    public static final String REDIS_SERVER_TEST_ON_RETURN = "false";
    public static final String REDIS_SERVER_TEST_WHILE_IDLE = "false";
    public static final String REDIS_SERVER_MIN_EVICT_IDL_TIME = "10000";
    public static final String REDIS_SERVER_TIME_BW_EVCT_RUNS = "-1";
    public static final String REDIS_SERVER_NUM_TESTS_PER_EVCT_RUN = "3";
    public static final String REDIS_SERVER_BLOCK_WHEN_EXHAUSTED = "false";
}