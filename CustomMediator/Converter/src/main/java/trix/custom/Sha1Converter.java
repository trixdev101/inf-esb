package trix.custom;

import org.apache.synapse.MessageContext; 
import org.apache.synapse.mediators.AbstractMediator;

import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Sha1Converter extends AbstractMediator { 
	private static final Log log = LogFactory.getLog(AbstractMediator.class);
	private String targetString;
	
	public String getTargetString() {
		return targetString;
	}

	public void setTargetString(String targetString) {
		this.targetString = targetString;
	}

	public boolean mediate(MessageContext context) {
		
		if(targetString != null && targetString != "")
		{
			String result = Base64.getEncoder().encodeToString(DigestUtils.sha1(targetString));
			//log.debug(result);
			context.setProperty("convertResult", result);
		}
		 
		// TODO Implement your mediation logic here 
		return true;
	}
}
