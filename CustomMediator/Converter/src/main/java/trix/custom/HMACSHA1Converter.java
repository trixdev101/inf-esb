package trix.custom;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class HMACSHA1Converter extends AbstractMediator{
	private static final Log log = LogFactory.getLog(AbstractMediator.class);
	private String targetString;
	private String secretKey;
	
	public String getTargetString() {
		return targetString;
	}

	public void setTargetString(String targetString) {
		this.targetString = targetString;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String key) {
		this.secretKey = key;
	}

	@Override
	public boolean mediate(MessageContext context) {

		log.info("enter custom info" + targetString + secretKey);
		log.debug("enter custom debug" + targetString + secretKey);
		if(targetString != null && targetString != "" && secretKey != null && secretKey != "")
		{
			try {
				context.setProperty("convertResult",  GetHMACSHA1Signature(targetString, secretKey));
			} catch (Exception e) {
				context.setProperty("convertResult","null");
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public static String GetHMACSHA1Signature(String rawData, String secretKey) throws
	Exception
	{
		log.info("enter conversion logic");
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(signingKey);
		
		byte[] hashedValue = mac.doFinal(rawData.getBytes());
		String result = URLEncoder.encode(Base64.getEncoder().encodeToString(hashedValue), StandardCharsets.UTF_8.toString()) ;
		log.info("result " + result);
		return result;
	}
}
